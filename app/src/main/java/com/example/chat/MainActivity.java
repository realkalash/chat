package com.example.chat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private static final int SIGN_IN_REQUEST_CODE = 0;
    Button button;
    EditText editText;
    ConstraintLayout constraintRoot;

    private FirebaseListAdapter<ChatMessage> adapter;
    ListView listOfMessages;

    EditText input;
    Button fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        constraintRoot = findViewById(R.id.constraintRoot);
        fab = findViewById(R.id.button);
        input = (EditText)findViewById(R.id.editText);
        listOfMessages = (ListView) findViewById(R.id.listView);


        AuthorisationFirebase();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              FirebaseUser currentUser =  FirebaseAuth.getInstance().getCurrentUser();

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference()
                        .push()
                        .setValue(new ChatMessage(currentUser.getDisplayName()
                                , input.getText().toString() )
                        );

                // Clear the input
                input.setText("");
            }
        });
    }

    private void AuthorisationFirebase() {
        if(FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .build(),
                    SIGN_IN_REQUEST_CODE
            );
        } else {
            // User is already signed in. Therefore, display
            // a welcome Toast

            Snackbar.make(constraintRoot, "Welcome " + FirebaseAuth.getInstance()
                    .getCurrentUser()
                    .getDisplayName()
                    , Snackbar.LENGTH_SHORT).show();

            // Load chat room contents
            displayChatMessages();
        }
    }

    private void displayChatMessages() {


        adapter = new FirebaseListAdapter<ChatMessage>(this, ChatMessage.class,
                R.layout.message, FirebaseDatabase.getInstance().getReference()) {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {
                // Get references to the views of message.xml
                TextView messageText = (TextView)v.findViewById(R.id.m_text);
                TextView messageUser = (TextView)v.findViewById(R.id.m_name);
                TextView messageTime = (TextView)v.findViewById(R.id.m_date);
                ImageView photoContact = v.findViewById(R.id.m_photo);

                // Set their text
                messageText.setText(model.getText());
                messageUser.setText(model.getName());

                Glide.with(mContext).load(R.drawable.ic_contact).error(R.drawable.ic_launcher_foreground).into(photoContact);

//                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM (HH:mm)",
                        model.getDate()));
            }
        };

        listOfMessages.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGN_IN_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {

                Snackbar.make(constraintRoot, "Successfully signed in. Welcome!", Snackbar.LENGTH_SHORT).show();

                displayChatMessages();
            } else {

                Snackbar.make(constraintRoot, "We couldn't sign you in. Please try again later", Snackbar.LENGTH_SHORT)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AuthorisationFirebase();
                            }
                        }).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(MainActivity.this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(MainActivity.this,
                                    "You have been signed out.",
                                    Toast.LENGTH_LONG)
                                    .show();

                            // Close activity
                            finish();
                        }
                    });
        }
        return true;
    }
}
