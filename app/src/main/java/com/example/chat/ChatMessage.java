package com.example.chat;

import android.net.Uri;

import java.util.Date;

public class ChatMessage {

    private String name;
    private String text;
    private long date;
    private Uri photoUrl;

    public ChatMessage() {
    }

    public ChatMessage(String name, String text) {
        this.name = name;
        this.text = text;
        this.date = new Date().getTime();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getDate() {
        return date;
    }

    public Uri getPhotoUrl() {
        return photoUrl;
    }
}
